import { Component, OnInit } from '@angular/core';
import { Router, Routes } from '@angular/router';

import { Address } from './address';
import { Consignment } from './consignment';
import { ConsignmentDetailComponent } from './consignment-detail.component';
import { ConsignmentService } from './consignment.service';

@Component({
  selector: 'consignment-overview',
  templateUrl: './consignment-overview.component.html',
  styleUrls: [ './consignment-overview.component.css' ]
})

export class ConsignmentOverviewComponent implements OnInit {
  consignments         : Consignment[];
  selectedConsignment  : Consignment;  
  
  constructor(private consignmentService: ConsignmentService, private router: Router) { }

  getConsignments(): void {
    this.consignmentService.getConsignments().then(consignments => {
      this.consignments = consignments;
    });
  }
  
  ngOnInit(): void {
    this.getConsignments();
  }
  
  onSelect(consignment: Consignment): void {
    this.selectedConsignment = consignment;
  }
  
  gotoDetail(): void {
    this.router.navigate(['/consignment', this.selectedConsignment.id]);
  }
}