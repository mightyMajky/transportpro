import { Component } from '@angular/core';


@Component({
  selector: 'my-app',
  template: `
    <h2>Angular Consignments and Parcels</h2>
    <nav>
	    <a routerLink="/consignments" routerLinkActive="active" >Consignments</a>
	    <a routerLink="/parcels" routerLinkActive="active">Parcels</a>
	  </nav>
	  <router-outlet></router-outlet>
    `
})
export class AppComponent {
  
}
