"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var consignment_service_1 = require("./consignment.service");
var ConsignmentOverviewComponent = (function () {
    function ConsignmentOverviewComponent(consignmentService, router) {
        this.consignmentService = consignmentService;
        this.router = router;
    }
    ConsignmentOverviewComponent.prototype.getConsignments = function () {
        var _this = this;
        this.consignmentService.getConsignments().then(function (consignments) {
            _this.consignments = consignments;
        });
    };
    ConsignmentOverviewComponent.prototype.ngOnInit = function () {
        this.getConsignments();
    };
    ConsignmentOverviewComponent.prototype.onSelect = function (consignment) {
        this.selectedConsignment = consignment;
    };
    ConsignmentOverviewComponent.prototype.gotoDetail = function () {
        this.router.navigate(['/consignment', this.selectedConsignment.id]);
    };
    return ConsignmentOverviewComponent;
}());
ConsignmentOverviewComponent = __decorate([
    core_1.Component({
        selector: 'consignment-overview',
        templateUrl: './consignment-overview.component.html',
        styleUrls: ['./consignment-overview.component.css']
    }),
    __metadata("design:paramtypes", [consignment_service_1.ConsignmentService, router_1.Router])
], ConsignmentOverviewComponent);
exports.ConsignmentOverviewComponent = ConsignmentOverviewComponent;
//# sourceMappingURL=consignment-overview.component.js.map