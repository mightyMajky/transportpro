import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { ConsignmentService }             from './consignment.service';
import { ParcelService }                  from './parcel.service';
import { ConsignmentOverviewComponent }   from './consignment-overview.component';
import { ParcelOverviewComponent }        from './parcel-overview.component';
import { ConsignmentDetailComponent }     from './consignment-detail.component';
//import { ParcelDetailComponent }  		  from './parcel-detail.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent }  from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AppRoutingModule
  ],
  declarations	 : [
    AppComponent,
    ConsignmentOverviewComponent,
    ParcelOverviewComponent,
    ConsignmentDetailComponent,
    //ParcelDetailComponent
  ],
  providers     : [ ConsignmentService,ParcelService ],
  bootstrap     : [ AppComponent,
                   
                  ]
})
export class AppModule { }
