"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var consignment_overview_component_1 = require("./consignment-overview.component");
var parcel_overview_component_1 = require("./parcel-overview.component");
var consignment_detail_component_1 = require("./consignment-detail.component");
//import { ParcelDetailComponent }  			from './parcel-detail.component';
var routes = [
    { path: '', redirectTo: '/consignments', pathMatch: 'full' },
    { path: 'consignments', component: consignment_overview_component_1.ConsignmentOverviewComponent },
    { path: 'parcels', component: parcel_overview_component_1.ParcelOverviewComponent },
    { path: 'consignment/:id', component: consignment_detail_component_1.ConsignmentDetailComponent },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(routes)],
        exports: [router_1.RouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map