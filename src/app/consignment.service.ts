import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Consignment } from './consignment';

@Injectable()
export class ConsignmentService {
  private consignmentsUrl = 'http://localhost:8080/AngularTest1/rest/consignments';  
  //private consignmentsUrl = 'AngularTest1/rest/consignments';  

  constructor(private http: Http) { }

  getConsignments(): Promise<Consignment[]> {
    return this.http.get(this.consignmentsUrl)
               .toPromise()
               .then(response => response.json() as Consignment[])
               .catch(this.handleError);
  }
  
  getConsignment(id: number): Promise<Consignment> {
    return this.getConsignments().then(consignments => consignments.find(consignment => consignment.id === id));
  }  
  
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}