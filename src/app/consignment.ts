import { Address } from './address';

export class Consignment {
  id               : number;
  
  sender           : Address;
  receiver         : Address;  
        
  reference        : string;  
  clientNote       : string;    
  driverNote       : string;    
  receiverNote     : string;        
}