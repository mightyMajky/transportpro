export class Address {
  name       : string;
  street     : string;  
  town       : string;    
  zip        : string;  
  country    : number;    
  contact    : string;
  phone      : string;
  email      : string;
}