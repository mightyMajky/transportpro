export class Parcel {
  id                : number;
  consignmentId     : number;
  
  note        : string;
  weight      : string;
  barcode     : string;

  cod         : number;
  codValue    : number;
  codSymbol   : string;
}