import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { ConsignmentService } from './consignment.service';
import { Consignment } from './consignment';

@Component({
  selector: 'consignment-detail',
  templateUrl: './consignment-detail.component.html',
  styleUrls: [ './consignment-detail.component.css' ]
})

export class ConsignmentDetailComponent {
  @Input() consignment   : Consignment;
  @Input() omitList      : number;

  constructor(
    private consignmentService: ConsignmentService,
    private route: ActivatedRoute,
    private location: Location
  ) {}
  
  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.consignmentService.getConsignment(+params['id']))
      .subscribe(consignment => {
        if (consignment)
          this.consignment = consignment;
      });
  
  }
  
  goBack(): void {
    this.location.back();
  }
}