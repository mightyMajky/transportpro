import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Parcel } from './parcel';

@Injectable()
export class ParcelService {
  private parcelsUrl = 'http://localhost:8080/AngularTest1/rest/parcels';  
  //private parcelsUrl = 'AngularTest1/rest/parcels';    

  constructor(private http: Http) { }

  getParcels(): Promise<Parcel[]> {
    return this.http.get(this.parcelsUrl)
               .toPromise()
               .then(response => response.json() as Parcel[])
               .catch(this.handleError);
  }
  
  
  getConsignmentParcels(consigId: number): Promise<Parcel[]> {

    return this.getParcels().then(parcels => parcels.filter(function(parcel) {
      return parcel.consignmentId == consigId;
    }));
  } 
  
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}