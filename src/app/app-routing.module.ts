import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConsignmentOverviewComponent }   from './consignment-overview.component';
import { ParcelOverviewComponent }      	from './parcel-overview.component';
import { ConsignmentDetailComponent }  	from './consignment-detail.component';
//import { ParcelDetailComponent }  			from './parcel-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/consignments', pathMatch: 'full' },
  { path: 'consignments',  	component: ConsignmentOverviewComponent },
  { path: 'parcels',  			component: ParcelOverviewComponent },  
  { path: 'consignment/:id', 	component: ConsignmentDetailComponent },
  //{ path: 'parcel/:id', 		component: ParcelDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}