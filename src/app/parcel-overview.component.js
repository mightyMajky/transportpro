"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var consignment_1 = require("./consignment");
var parcel_service_1 = require("./parcel.service");
var consignment_service_1 = require("./consignment.service");
var ParcelOverviewComponent = (function () {
    function ParcelOverviewComponent(parcelService, consignmentService, route, location) {
        this.parcelService = parcelService;
        this.consignmentService = consignmentService;
        this.route = route;
        this.location = location;
        this.omitConsignmentDeatil = 0;
    }
    ParcelOverviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.consig) {
            this.parcelService.getConsignmentParcels(this.consig.id).then(function (parcels) {
                _this.parcels = parcels;
            });
        }
        else {
            this.parcelService.getParcels().then(function (parcels) {
                _this.parcels = parcels;
            });
        }
    };
    ParcelOverviewComponent.prototype.onSelect = function (parcel) {
        var _this = this;
        if (this.omitConsignmentDeatil == 0) {
            this.selectedParcel = parcel;
            this.consignmentService.getConsignment(parcel.consignmentId).then(function (consignment) {
                _this.consig = consignment;
            });
        }
    };
    ParcelOverviewComponent.prototype.goBack = function () {
        this.location.back();
    };
    return ParcelOverviewComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ParcelOverviewComponent.prototype, "omitConsignmentDeatil", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", consignment_1.Consignment)
], ParcelOverviewComponent.prototype, "consig", void 0);
ParcelOverviewComponent = __decorate([
    core_1.Component({
        selector: 'parcel-overview',
        templateUrl: './parcel-overview.component.html',
        styleUrls: ['./parcel-overview.component.css']
    }),
    __metadata("design:paramtypes", [parcel_service_1.ParcelService,
        consignment_service_1.ConsignmentService,
        router_1.ActivatedRoute,
        common_1.Location])
], ParcelOverviewComponent);
exports.ParcelOverviewComponent = ParcelOverviewComponent;
//# sourceMappingURL=parcel-overview.component.js.map