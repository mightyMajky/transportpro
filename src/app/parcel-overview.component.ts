import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import { Parcel } from './parcel';
import { Consignment } from './consignment';
import { ParcelService } from './parcel.service';
import { ConsignmentService } from './consignment.service';

@Component({
  selector: 'parcel-overview',
  templateUrl: './parcel-overview.component.html',
  styleUrls: [ './parcel-overview.component.css' ]
})

export class ParcelOverviewComponent {
  parcels              : Parcel[];
  selectedParcel       : Parcel;
  @Input() omitConsignmentDeatil: number;
  @Input() consig      : Consignment;
  
  constructor(private parcelService: ParcelService,
              private consignmentService: ConsignmentService,
			     private route: ActivatedRoute,
			     private location: Location
  ) {
    this.omitConsignmentDeatil = 0;
  }
  
  ngOnInit(): void {
    if (this.consig) {      
      this.parcelService.getConsignmentParcels(this.consig.id).then(parcels => {
	     this.parcels = parcels;
	   });
    }
    else {
    	this.parcelService.getParcels().then(parcels => {
	     this.parcels = parcels;
	   });
    }
  }
  
  onSelect(parcel: Parcel): void {
    if (this.omitConsignmentDeatil == 0) {    	    
	    this.selectedParcel = parcel;
	    this.consignmentService.getConsignment(parcel.consignmentId).then(consignment => {     
	      this.consig = consignment;
	    });
	 }
  }

  goBack(): void {
    this.location.back();
  }
}