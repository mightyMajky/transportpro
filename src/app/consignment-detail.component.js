"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
require("rxjs/add/operator/switchMap");
var consignment_service_1 = require("./consignment.service");
var consignment_1 = require("./consignment");
var ConsignmentDetailComponent = (function () {
    function ConsignmentDetailComponent(consignmentService, route, location) {
        this.consignmentService = consignmentService;
        this.route = route;
        this.location = location;
    }
    ConsignmentDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .switchMap(function (params) { return _this.consignmentService.getConsignment(+params['id']); })
            .subscribe(function (consignment) {
            if (consignment)
                _this.consignment = consignment;
        });
    };
    ConsignmentDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    return ConsignmentDetailComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", consignment_1.Consignment)
], ConsignmentDetailComponent.prototype, "consignment", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ConsignmentDetailComponent.prototype, "omitList", void 0);
ConsignmentDetailComponent = __decorate([
    core_1.Component({
        selector: 'consignment-detail',
        templateUrl: './consignment-detail.component.html',
        styleUrls: ['./consignment-detail.component.css']
    }),
    __metadata("design:paramtypes", [consignment_service_1.ConsignmentService,
        router_1.ActivatedRoute,
        common_1.Location])
], ConsignmentDetailComponent);
exports.ConsignmentDetailComponent = ConsignmentDetailComponent;
//# sourceMappingURL=consignment-detail.component.js.map