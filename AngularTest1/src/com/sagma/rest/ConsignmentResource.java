package com.sagma.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.sagma.domain.Consignment;
import com.sagma.stores.ConsignmentStore;

public class ConsignmentResource {
	@Context
    UriInfo uriInfo;
    @Context
    Request request;
    String consignmentID;
     
    public ConsignmentResource(UriInfo uriInfo, Request request, String consignmentID) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.consignmentID = consignmentID;
    }
     
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Consignment getConsignment() {
    	return ConsignmentStore.getConsignment(consignmentID);
    }
}
