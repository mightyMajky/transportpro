package com.sagma.rest;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.sagma.domain.Consignment;
import com.sagma.stores.ConsignmentStore;

@Path("/consignments")
public class ConsignmentsResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Consignment> getConsignments(@Context HttpHeaders header, 
    										 @Context HttpServletResponse response) {
    	
    	response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    		
        return ConsignmentStore.getConsignments();
    }
    
    @Path("{consignment}")
    public ConsignmentResource getConsignment(@PathParam("consignment") String consignmentId) {
        return new ConsignmentResource(uriInfo, request, consignmentId);
    }
}
