package com.sagma.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.sagma.domain.Parcel;
import com.sagma.stores.ParcelStore;

public class ParcelResource {
	@Context
    UriInfo uriInfo;
    @Context
    Request request;
    String parcelID;
     
    public ParcelResource(UriInfo uriInfo, Request request, String parcelID) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.parcelID = parcelID;
    }
     
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Parcel getParcel() {
    	return ParcelStore.getParcel(parcelID);
    }
}
