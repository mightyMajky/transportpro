package com.sagma.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.sagma.domain.Parcel;
import com.sagma.util.DBUtils;

public class ParcelStore {
	public static final List<Parcel> getParcels() {
		List<Parcel> list = new ArrayList<Parcel>();
		Connection connection 	= null;
		PreparedStatement stmt 	= null; 
		ResultSet rs			= null;
		try {
			connection 	= DBUtils.getConnection();
			stmt 		= connection.prepareStatement("select * from parcel");
			rs = stmt.executeQuery();
			while (rs.next()) {
				Parcel parcel = new Parcel();
				parcel.setId(rs.getLong("id"));
				parcel.setConsignmentId(rs.getLong("consignmentId"));
				
				parcel.setNote(rs.getString("note"));
				parcel.setWeight(rs.getString("weight"));
				parcel.setBarcode(rs.getString("barcode"));
				
				parcel.setCod(rs.getInt("cod"));
				parcel.setCodValue(rs.getString("codValue"));
				parcel.setCodSymbol(rs.getString("codSymbol"));
				
				list.add(parcel);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(connection, stmt, rs);
		}
		return list;
	}
	
	public static final Parcel getParcel(String parcelId) {
		Connection connection 	= null;
		PreparedStatement stmt 	= null; 
		ResultSet rs			= null;
		try {
			connection 	= DBUtils.getConnection();
			stmt 		= connection.prepareStatement("select * from parcel where id=?");
			stmt.setString(1, parcelId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				Parcel parcel = new Parcel();
				parcel.setId(rs.getLong("id"));
				parcel.setConsignmentId(rs.getLong("consignmentId"));
				
				parcel.setNote(rs.getString("note"));
				parcel.setWeight(rs.getString("weight"));
				parcel.setBarcode(rs.getString("barcode"));
				
				parcel.setCod(rs.getInt("cod"));
				parcel.setCodValue(rs.getString("codValue"));
				parcel.setCodSymbol(rs.getString("codSymbol"));
				
				return parcel;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtils.closeConnection(connection, stmt, rs);
		}
	}
}
