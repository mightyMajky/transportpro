package com.sagma.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.sagma.domain.Consignment;
import com.sagma.util.DBUtils;

public class ConsignmentStore {
	public static final List<Consignment> getConsignments() {
		List<Consignment> list = new ArrayList<Consignment>();
		Connection connection 	= null;
		PreparedStatement stmt 	= null; 
		ResultSet rs			= null;
		try {
			connection 	= DBUtils.getConnection();
			stmt 		= connection.prepareStatement("select * from consignment");
			rs = stmt.executeQuery();
			while (rs.next()) {
				Consignment consignment = new Consignment();
				consignment.setId(rs.getLong("id"));
				
				consignment.getSender().setName(rs.getString("senderName"));
				consignment.getSender().setStreet(rs.getString("senderStreet"));
				consignment.getSender().setTown(rs.getString("senderTown"));
				consignment.getSender().setZip(rs.getString("senderZip"));
				consignment.getSender().setCountry(rs.getInt("senderCountry"));
				consignment.getSender().setContact(rs.getString("senderContact"));
				consignment.getSender().setPhone(rs.getString("senderPhone"));
				consignment.getSender().setEmail(rs.getString("senderEmail"));
				
				consignment.getReceiver().setName(rs.getString("receiverName"));
				consignment.getReceiver().setStreet(rs.getString("receiverStreet"));
				consignment.getReceiver().setTown(rs.getString("receiverTown"));
				consignment.getReceiver().setZip(rs.getString("receiverZip"));
				consignment.getReceiver().setCountry(rs.getInt("receiverCountry"));
				consignment.getReceiver().setContact(rs.getString("receiverContact"));
				consignment.getReceiver().setPhone(rs.getString("receiverPhone"));
				consignment.getReceiver().setEmail(rs.getString("receiverEmail"));
				
				consignment.setReference(rs.getString("reference"));
				consignment.setClientNote(rs.getString("clientNote"));
				consignment.setDriverNote(rs.getString("driverNote"));
				consignment.setReceiverNote(rs.getString("receiverNote"));
				
				list.add(consignment);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtils.closeConnection(connection, stmt, rs);
		}
		return list;
	}
	
	public static final Consignment getConsignment(String consignmentId) {
		Connection connection 	= null;
		PreparedStatement stmt 	= null; 
		ResultSet rs			= null;
		try {
			connection 	= DBUtils.getConnection();
			stmt 		= connection.prepareStatement("select * from consignment where id=?");
			stmt.setString(1, consignmentId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				Consignment consignment = new Consignment();
				consignment.setId(rs.getLong("id"));
				
				consignment.getSender().setName(rs.getString("senderName"));
				consignment.getSender().setStreet(rs.getString("senderStreet"));
				consignment.getSender().setTown(rs.getString("senderTown"));
				consignment.getSender().setZip(rs.getString("senderZip"));
				consignment.getSender().setCountry(rs.getInt("senderCountry"));
				consignment.getSender().setContact(rs.getString("senderContact"));
				consignment.getSender().setPhone(rs.getString("senderPhone"));
				consignment.getSender().setEmail(rs.getString("senderEmail"));
				
				consignment.getReceiver().setName(rs.getString("receiverName"));
				consignment.getReceiver().setStreet(rs.getString("receiverStreet"));
				consignment.getReceiver().setTown(rs.getString("receiverTown"));
				consignment.getReceiver().setZip(rs.getString("receiverZip"));
				consignment.getReceiver().setCountry(rs.getInt("receiverCountry"));
				consignment.getReceiver().setContact(rs.getString("receiverContact"));
				consignment.getReceiver().setPhone(rs.getString("receiverPhone"));
				consignment.getReceiver().setEmail(rs.getString("receiverEmail"));
				
				consignment.setReference(rs.getString("reference"));
				consignment.setClientNote(rs.getString("clientNote"));
				consignment.setDriverNote(rs.getString("driverNote"));
				consignment.setReceiverNote(rs.getString("receiverNote"));
				
				return consignment;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			DBUtils.closeConnection(connection, stmt, rs);
		}
	}
}
