package com.sagma.domain;

public class Consignment {
	long id;

	Address sender   = new Address();
	Address receiver = new Address();
	  
	String reference;  
	String clientNote;    
	String driverNote;    
	String receiverNote;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Address getSender() {
		return sender;
	}
	public void setSender(Address sender) {
		this.sender = sender;
	}
	public Address getReceiver() {
		return receiver;
	}
	public void setReceiver(Address receiver) {
		this.receiver = receiver;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getClientNote() {
		return clientNote;
	}
	public void setClientNote(String clientNote) {
		this.clientNote = clientNote;
	}
	public String getDriverNote() {
		return driverNote;
	}
	public void setDriverNote(String driverNote) {
		this.driverNote = driverNote;
	}
	public String getReceiverNote() {
		return receiverNote;
	}
	public void setReceiverNote(String receiverNote) {
		this.receiverNote = receiverNote;
	}
}
