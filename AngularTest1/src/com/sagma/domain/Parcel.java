package com.sagma.domain;

public class Parcel {
	long id;
	long consignmentId;

	String note;
	String weight;
	String barcode;
	
	int cod;
	String codValue;
	String codSymbol;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	public String getCodValue() {
		return codValue;
	}
	public void setCodValue(String codValue) {
		this.codValue = codValue;
	}
	public String getCodSymbol() {
		return codSymbol;
	}
	public void setCodSymbol(String codSymbol) {
		this.codSymbol = codSymbol;
	}
	public long getConsignmentId() {
		return consignmentId;
	}
	public void setConsignmentId(long consignmentId) {
		this.consignmentId = consignmentId;
	}
}
